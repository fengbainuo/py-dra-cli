import sys

import requests
from bs4 import BeautifulSoup


# Get the current working URL
def get_url():
    base_url = "https://dramacool.info"
    response = requests.get(base_url, allow_redirects=True)
    current_url = response.url
    if current_url != base_url:
        # print(f"Current server: {current_url}")
        return current_url


# Get the title from the user
def user_search():
    title = input("Drama title: ")
    return title


# Get the content of a website
def get_webcontent(url, title):
    search_url = f"{url}search.html?keyword={title}"
    # print(search_url)
    response = requests.get(search_url)
    soup = BeautifulSoup(response.text, 'html.parser')
    tag_title = soup.find_all("div", class_='name')
    titles = [div.get_text(strip=True) for div in tag_title]
    return titles


def user_drama_select(titles):
    a = 1
    for i in titles:
        print(f"{a} - {i}")
        a += 1
    print("q - Exit")

    chosen = input("Select drama: ")
    if chosen.lower() == "q":
        sys.exit()
    else:
        selected_drama = titles[int(chosen) - 1]
    selected_drama = selected_drama.lower().replace(" ", "-").replace("(", "").replace(")", "")
    return selected_drama


def create_episode_list(url, drama):
    for i in range(int(drama.split('-')[-1])):
        print(f"{i+1} - Episode {i+1}")
    print("q - Quit")
    drama = drama.split('-')
    drama.pop()
    drama = '-'.join(drama)
    episode = input("Select episode number: ")
    if episode.lower() == "q":
        sys.exit()
    else:
        episode_url = f"{url}videos/{drama}-{episode}"
    print(episode_url)


create_episode_list(get_url(), user_drama_select(get_webcontent(get_url(), user_search())))
